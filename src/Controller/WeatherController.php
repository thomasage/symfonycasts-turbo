<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(
    '/weather',
    name: 'app_weather'
)]
final class WeatherController extends AbstractController
{
    public function __invoke(): Response
    {
        return $this->render('weather/index.html.twig');
    }
}
