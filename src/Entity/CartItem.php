<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @Assert\Callback("validateColor")
 */
class CartItem
{
    private Product $product;

    private ?Color $color;

    /**
     * @Assert\GreaterThanOrEqual(1, message="Enter a quantity greater than 0")
     * @Assert\NotBlank(message="Please enter a valid quantity")
     */
    private int $quantity;

    public function __construct(Product $product, int $quantity = 1, Color $color = null)
    {
        $this->product = $product;
        $this->quantity = $quantity;
        $this->color = $color;
    }

    public function increaseQuantity(int $quantity): void
    {
        $this->quantity += $quantity;
    }

    public function matches(CartItem $cartItem): bool
    {
        $color = $this->getColor();
        $thisKey = sprintf(
            '%s_%s',
            $this->getProduct()->getId(),
            $color ? $color->getId() : 'no_color'
        );
        $color = $cartItem->getColor();
        $thatKey = sprintf(
            '%s_%s',
            $cartItem->getProduct()->getId(),
            $color ? $color->getId() : 'no_color'
        );

        return $thisKey === $thatKey;
    }

    public function getColor(): ?Color
    {
        return $this->color;
    }

    public function setColor(Color $color): void
    {
        $this->color = $color;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function validateColor(ExecutionContextInterface $context): void
    {
        if (!$this->product->hasColors()) {
            return;
        }

        if (!$this->color) {
            $context
                ->buildViolation('Please select a color')
                ->atPath('color')
                ->addViolation();
        }
    }

    public function getTotalString(): string
    {
        return (string) ($this->getTotal() / 100);
    }

    public function getTotal(): int
    {
        return $this->getProduct()->getPrice() * $this->getQuantity();
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function createPurchaseItem(): PurchaseItem
    {
        $purchaseItem = new PurchaseItem();
        $purchaseItem->setProduct($this->product);
        $purchaseItem->setQuantity($this->quantity);
        $purchaseItem->setColor($this->color);

        return $purchaseItem;
    }

    public function getIdentifier(): string
    {
        return sprintf(
            '%s_%s',
            $this->product->getId(),
            $this->color ? $this->color->getId() : ''
        );
    }
}
